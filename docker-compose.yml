version: "2.9"
services:
  ouroboros:
    image: pyouroboros/ouroboros
    hostname: ouroboros
    container_name: ouroboros
    environment:
      CLEANUP: true
      INTERVAL: 300
      LOG_LEVEL: info
      SELF_UPDATE: true
      TZ: Europe/Moscow
    restart: unless-stopped
    volumes:
      - //var/run/docker.sock:/var/run/docker.sock

  postgres:
    image: postgres:latest
    container_name: dturpostgres
    hostname: dturpostgres
    restart: unless-stopped
    ports:
      - "5432:5432"
    volumes:
      - $POSTGRES_VOL
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DB: postgres
    healthcheck:
      test: [ "CMD", "bash", "-c", "echo postgres" ]
      interval: 5s
      timeout: 5s

  liquibase:
    image: liquibase/liquibase:latest
    container_name: dturliquibase
    hostname: dturliquibase
    command: $LQB_COMMAND
    volumes:
      - $LQB_VOL
    depends_on:
      postgres:
        condition: service_healthy
    healthcheck:
      test: [ "CMD", "bash", "-c", "echo liquibase" ]
      interval: 5s
      timeout: 5s

  mongo:
    image: mongo
    container_name: dturmongo
    hostname: dturmongo
    restart: unless-stopped
    ports:
      - "27017:27017"
    volumes:
      - $MONGO_VOL
    healthcheck:
      test: [ "CMD", "bash", "-c", "echo mongo" ]
      interval: 5s
      timeout: 5s

  balancer:
    image: registry.gitlab.com/denisturovsky/task-manager-73/task-manager-balancer
    hostname: dturcluster
    container_name: dturcluster
    ports:
      - "80:80"
    environment:
      TM_CLUSTER: dturcluster
      TM_ALPHA_HOST: http://dturserver-alpha
      TM_ALPHA_PORT: 8080
      TM_BETTA_HOST: http://dturserver-betta
      TM_BETTA_PORT: 8080
      TM_GAMMA_HOST: http://dturserver-gamma
      TM_GAMMA_PORT: 8080

  alpha:
    image: registry.gitlab.com/denisturovsky/task-manager-73/task-manager-server
    container_name: dturserver-alpha
    hostname: dturserver-alpha
    depends_on:
      logger-test:
        condition: service_healthy
      postgres:
        condition: service_healthy
      liquibase:
        condition: service_completed_successfully
    environment:
      TM_DB_URL: jdbc:postgresql://dturpostgres:5432/postgres
      TM_JMS_HOST: dturserver-alpha
      TM_JMS_PORT: 61616
      TM_SRV_HOST: dturserver-alpha
    healthcheck:
      test: [ "CMD", "bash", "-c", "echo alpha" ]
      interval: 15s
      timeout: 15s

  betta:
    image: registry.gitlab.com/denisturovsky/task-manager-73/task-manager-server
    container_name: dturserver-betta
    hostname: dturserver-betta
    depends_on:
      logger-test:
        condition: service_healthy
      postgres:
        condition: service_healthy
      liquibase:
        condition: service_completed_successfully
    environment:
      TM_DB_URL: jdbc:postgresql://dturpostgres:5432/postgres
      TM_JMS_HOST: dturserver-betta
      TM_JMS_PORT: 61616
      TM_SRV_HOST: dturserver-betta
    healthcheck:
      test: [ "CMD", "bash", "-c", "echo betta" ]
      interval: 15s
      timeout: 15s

  gamma:
    image: registry.gitlab.com/denisturovsky/task-manager-73/task-manager-server
    container_name: dturserver-gamma
    hostname: dturserver-gamma
    depends_on:
      logger-test:
        condition: service_healthy
      postgres:
        condition: service_healthy
      liquibase:
        condition: service_completed_successfully
    environment:
      TM_DB_URL: jdbc:postgresql://dturpostgres:5432/postgres
      TM_JMS_HOST: dturserver-gamma
      TM_JMS_PORT: 61616
      TM_SRV_HOST: dturserver-gamma
    healthcheck:
      test: [ "CMD", "bash", "-c", "echo gamma" ]
      interval: 15s
      timeout: 15s

  logger-alpha:
    image: registry.gitlab.com/denisturovsky/task-manager-73/task-manager-logger
    container_name: dturlogger-alpha
    hostname: dturlogger-alpha
    depends_on:
      alpha:
        condition: service_healthy
      mongo:
        condition: service_healthy
    environment:
      TM_JMS_HOST: dturserver-alpha
      TM_JMS_PORT: 61616
      TM_MONGO_HOST: dturmongo
      TM_MONGO_PORT: 27017
    restart: on-failure

  logger-betta:
    image: registry.gitlab.com/denisturovsky/task-manager-73/task-manager-logger
    container_name: dturlogger-betta
    hostname: dturlogger-betta
    depends_on:
      betta:
        condition: service_healthy
      mongo:
        condition: service_healthy
    environment:
      TM_JMS_HOST: dturserver-betta
      TM_JMS_PORT: 61616
      TM_MONGO_HOST: dturmongo
      TM_MONGO_PORT: 27017
    restart: on-failure

  logger-gamma:
    image: registry.gitlab.com/denisturovsky/task-manager-73/task-manager-logger
    container_name: dturlogger-gamma
    hostname: dturlogger-gamma
    depends_on:
      gamma:
        condition: service_healthy
      mongo:
        condition: service_healthy
    environment:
      TM_JMS_HOST: dturserver-gamma
      TM_JMS_PORT: 61616
      TM_MONGO_HOST: dturmongo
      TM_MONGO_PORT: 27017
    restart: on-failure

  server-test:
    image: registry.gitlab.com/denisturovsky/task-manager-73/task-manager-server
    container_name: dturserver-test
    hostname: dturserver-test
    depends_on:
      postgres:
        condition: service_healthy
      liquibase:
        condition: service_completed_successfully
    ports:
      - "8080:8080"
      - "61616:61616"
    environment:
      TM_DB_URL: jdbc:postgresql://dturpostgres:5432/postgres
      TM_JMS_HOST: dturserver-test
      TM_JMS_PORT: 61616
      TM_SRV_HOST: dturserver-test
    healthcheck:
      test: [ "CMD", "bash", "-c", "echo server-test" ]
      interval: 15s
      timeout: 15s

  logger-test:
    image: registry.gitlab.com/denisturovsky/task-manager-73/task-manager-logger
    container_name: dturlogger-test
    hostname: dturlogger-test
    depends_on:
      server-test:
        condition: service_healthy
      mongo:
        condition: service_healthy
    environment:
      TM_JMS_HOST: dturserver-test
      TM_JMS_PORT: 61616
      TM_MONGO_HOST: dturmongo
      TM_MONGO_PORT: 27017
    restart: on-failure
    healthcheck:
      test: [ "CMD", "bash", "-c", "echo logger-test" ]
      interval: 5s
      timeout: 5s

  postgres-web:
    image: postgres:latest
    container_name: dturpostgres-web
    hostname: dturpostgres-web
    restart: unless-stopped
    ports:
      - "5532:5432"
    volumes:
      - $POSTGRES_VOL_WEB
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DB: postgres
    healthcheck:
      test: [ "CMD", "bash", "-c", "echo postgres-web" ]
      interval: 5s
      timeout: 5s

  liquibase-web:
    image: liquibase/liquibase:latest
    container_name: dturliquibase-web
    hostname: dturliquibase-web
    command: $LQB_COMMAND_WEB
    volumes:
      - $LQB_VOL_WEB
    depends_on:
      postgres-web:
        condition: service_healthy
    healthcheck:
      test: [ "CMD", "bash", "-c", "echo liquibase-web" ]
      interval: 5s
      timeout: 5s

networks:
  default: